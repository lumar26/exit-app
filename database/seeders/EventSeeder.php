<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([[
            'name' => "Boris Brejcha",
            'start' => Carbon::create('2022', '7', '20'),
            'image' => "https://i.ibb.co/1vyjNrd/brejcha.jpg",
            'stage_id' => rand(1, 6),
            'user_id' => rand(1, 3)
        ],
            [
                'name' => "Nick Cave and the Bad Seeds",
                'start' => Carbon::create('2022', '7', '21'),
                'image' => "https://i.ibb.co/w4ZdTRy/nick-cave.jpg" ,
                'stage_id' => rand(1, 6),
                'user_id' => rand(1, 3)
            ]]);
    }
}
