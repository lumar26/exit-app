<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventPerformerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_performer')->insertOrIgnore($this->eventPerformerFactory(4)); // ukoliko se desi duplirani primarni kljuc onda cemo samo da ignorisemo
    }

    private function eventPerformerFactory(int $size = 5): array
    {
        $result = array();
        for ($i = 0; $i < $size; $i++) {
            $arr = [
                'event_id' => rand(1, 2),
                'performer_id' => rand(1, 15),
                'created_at' => now(),
                'updated_at' => now()
            ];
            array_push($result, $arr);
        }
        return $result;
    }
}
