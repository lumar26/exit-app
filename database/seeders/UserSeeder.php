<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => "admin",
            'email' => "admin@gmail.com",
            'password' => bcrypt("admin1234"),
            'role' => "admin",
        ],
            [
                'name' => "Luka Marinkovic",
                'email' => "luka@gmail.com",
                'password' => bcrypt("luka1234"),
                'role' => "admin",
            ],
            [
                'name' => "Lazar Marinkovic",
                'email' => "laza@gmail.com",
                'password' => bcrypt("admin"),
                'role' => "visitor",
            ],
            ]);
        User::factory(4)->create();

    }

}
