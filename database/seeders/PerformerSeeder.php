<?php

namespace Database\Seeders;

use App\Models\Performer;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PerformerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         * $table->string('name');
            $table->string('surname');
            $table->string('nick')->unique();
            $table->string('music_genre');
            $table->string('image');
            $table->foreignId('user_id')->constrained()->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();*/

//        DB::table('performers')->insert(Performer::factory(15)->create()->get());
//        Performer::factory(15)->create();
        DB::table('performers')->insert([[
            'name' => "Boris ",
            'surname' => "Brejcha",
            'nick' => 'Brejcha',
            'image' => "https://i.ibb.co/1vyjNrd/brejcha.jpg",
            'music_genre' => "High-Tech Minimal",
            'user_id' => rand(1, 3)
        ],
            [
                'name' => "Nicholas",
                'surname' => "Edward Cave",
                'nick' => 'Nick Cave',
                'image' => "https://i.ibb.co/w4ZdTRy/nick-cave.jpg",
                'music_genre' => "Post-Punk, Alternative rock",
                'user_id' => rand(1, 3)
            ],
            [
                'name' => "Luca",
                'surname' => "De Gregorio",
                'nick' => 'Meduza',
                'image' => "https://i.ibb.co/jJkGVL1/meduza.jpg",
                'music_genre' => "Electronic, Dance",
                'user_id' => rand(1, 3)
            ],
            [
                'name' => "Sonny",
                'surname' => "John Moore",
                'nick' => 'Skrillex',
                'image' => "https://i.ibb.co/LSzd6qs/skrillex.jpg",
                'music_genre' => "EDM, Dubstep, Electro house",
                'user_id' => rand(1, 3)
            ],
            [
                'name' => "Mladen",
                'surname' => "Coroddo Solomun",
                'nick' => 'Solomun',
                'image' => "https://i.ibb.co/MGGSx6f/solomun.jpg",
                'music_genre' => "Deep house, Melodic house",
                'user_id' => rand(1, 3)
            ]]);
    }

}
