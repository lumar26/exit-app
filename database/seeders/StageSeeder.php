<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        DB::table('stages')->insert([[
            'name' => "Main Stage",
            'location' => "Petrovaradin: center",
            'sponsor' => "Exit",
            'image' => "https://i.ibb.co/1rRk3kV/main-stage.jpg",
            'capacity' => random_int(5000, 15000),
            'user_id' => random_int(1, 3)
        ],
            [
                'name' => "Dance Arena",
                'location' => "Petrovaradin: east",
                'sponsor' => "mts",
                'image' => "https://i.ibb.co/Khk485c/mts-dance-arena.jpg",
                'capacity' => random_int(5000, 15000),
                'user_id' => random_int(1, 3)
            ],
            [
                'name' => "Fusion Stage",
                'location' => "Petrovaradin: west",
                'image' => "https://i.ibb.co/svy9c1r/visa-fusion-stage.jpg",
                'sponsor' => "VISA",
                'capacity' => random_int(5000, 15000),
                'user_id' => random_int(1, 3)
            ],
            [
                'name' => "Explosive Stage",
                'location' => "Petrovaradin: south",
                'sponsor' => "Explosive pub",
                'image' => "https://i.ibb.co/njtPFtt/explosive-pub.jpg",
                'capacity' => random_int(5000, 15000),
                'user_id' => random_int(1, 3)
            ],
            [
                'name' => "No Sleep Stage",
                'location' => "Petrovaradin: north",
                'sponsor' => "Guarana",
                'image' => "https://i.ibb.co/pffx1tr/no-sleep.jpg",
                'capacity' => random_int(5000, 15000),
                'user_id' => random_int(1, 3)
            ],
            [
                'name' => "Gang Beats Stage",
                'location' => "Petrovaradin: north-east",
                'sponsor' => "Noizz",
                'image' => "https://i.ibb.co/QMsBzKw/cockta-beats.jpg",
                'capacity' => random_int(5000, 15000),
                'user_id' => random_int(1, 3)
            ]]);
    }
}
