<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPerformerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_performer', function (Blueprint $table) {
            $table->foreignId('event_id')->constrained()->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('performer_id')->constrained()->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
            $table->primary(['event_id', 'performer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_performer');
        Schema::table('event_performer', function (Blueprint $table){
            $table->dropForeign('stage_id');
            $table->dropForeign('user_id');
        });
    }
}
