<?php

namespace Database\Factories;

use App\Models\Performer;
use Illuminate\Database\Eloquent\Factories\Factory;

class PerformerFactory extends Factory
{

    protected $model = Performer::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'nick' => $this->faker->name,
            'image' => "https://i.ibb.co/w4ZdTRy/nick-cave.jpg" ,
            'music_genre' => $this->faker->name,
            'user_id' => rand(1, 10)
        ];
    }
}
