#Uvod

Projektni zadatak iz predmeta Internet tehnologije, Katedra za Elektrosnko poslovanje, Fakultet Organizacionih Nauka, Univerzitet u Beogradu.

Aplikacija **_Exit App_** je web aplikacija koja nudi REST API (Application Programming Interface) 
za praćenje Exit festivala. Ova veb aplikacija nudi servis za skladištenje i upravljanje događajima, binama, 
izvođačima na festivalu kao i upravljanje kupljenim kartama od strane korisnika.  

#Pregled korišćenih tehnologija

Za ovaj projekat korišćeni su Laravel okvir za razvoj web aplikacija kao i MySQL sistem za upravljanje bazom podataka

## O Laravelu

Laravel je okvir otvorenog koda za razvoj Web aplikacija  otvorenog koda baziran na PHP programskom jeziku. Ovaj okvir je robustan
i lako razumljiv. Laravel prati MVC (model - view - controller) arhitekturalni patern projektovanja. Odlikuje se
upotrebom već postojećih delova koda nekih drugih okvira za razvoj web aplikacija što čini kreiranu aplikaciju 
bolje struktuiranom i praktičnijom za upotrebu.

### Prednosti Laravela

1. Skalabilnost web aplikacije
2. Ušteda vremena pri razvoju aplikacije
3. Uključuje interfejse i _namespace_-ove koji omogućavaju da se resursi u okviru aplikacije bolje organizuju

### Composer

Za rad sa Laravelom neophodno je koristit **Composer**, alat koji omogućava pristup svim neophodnim zavisnostima i 
bibliotekama. Sve zavisnosti su prikazane u fajlu `composer.json`.

### Artisan

Kao interfejs komandne linije Laravel koristi **Artisan**. Artisan sadrži set komandi koje inženjeru pomažu u razvoju aplikacije.
Većina komandi koje nudi Artisan su preuzete iz Simphony okvira.

###Ključne funkcionalnosti

Ključne funkcionalnosti koje nudi Laravel jesu:
- Modularnost: Laravel obezbeđuje 20 različitih ugrađenih biblioteka koje pomažu razvoju i unapređenju aplikacije.
- Testabilnost: Mogućnost testiranja aplikacije je ugrađena u Laravel kako bi proizvod mogao u svako doba biti u skladu sa korisničkim zahtevima
- Rutiranje: Omogućava korisniku da na lak način definiše rute 
- Query Builder & ORM: Alati za manipulaciju bazom podataka kao i za objektno-relaciono mapiranje
- Template Engine: Laravel koristi **Blade Template** mašinu za kreiranje grafičkog korisničkog interfejsa

##MySQL sistem za upravljanje bazom podataka

MySQL je sistem za upravljanje relacionim bazama podataka. To je softver otvorenog koda koji održava kompanija Oracle.
MySQl softver je besplatan ali je moguće kupiti i profesionalna izdanja kompanije Oracle.

MySQL podržava SQL upitni jezik za relacione baze podataka. 



#Model podataka

![Dijagram klasa](public/exit_app.drawio.png)

Na prikazanom modelu podataka uočavamo 5 entiteta: 
1. User: predstavlja korisnika koji je registrovan i koji se prijavljuje na sistem. Ukoliko 
korisnik ima ulogu administratora on može da izvršava CRUD operacije nad entitetima Event, Stage i Performer.
Korisnik koji ima ulogu posetioca može da ima uvid u bine, događaje i izvođače kao i mogućnost da kupi kartu za određenu binu.
2. Performer: predstavlja izvođača na festivalu. On ima samo jednog administratora koji ga je uneo u sistem. Jedan izvođač može učestvovati na više događaja i obrnuto.
3. Stage: predstavlja binu na festivalu. Svaka bina je uneta u sistem od strane jednog administratora.
4. Event: predstavlja jedan događaj u toku festivala. Događaj je unet u sistem od strane jednog administratora.
Svaki događaj se održava na tačno jednoj bini i može imati više izvođača.
5. Ticket: predstavlja jednu kupljenu kartu. Karta je vezana za određenu binu i može je kupiti samo jedan korisnik.

# REST API

U okviru ove web aplikacije, tj. web servisa definisane su API rute koje su dostupne za front-end inžewera koji razvija 
korisničku aplikaciju.

Dostupne rute su sledeće:

| Domain |  Method  |            URI             | Name |                           Action                           |                 Middleware                 |
|--------|:--------:|:--------------------------:|:----:|:----------------------------------------------------------:|:------------------------------------------:|
|        | HEAD/GET |             /              |      |                          Closure                           |                    web                     |
|        | GET/HEAD |         api/events         |      |         App\Http\Controllers\EventController@index         |                    api                     | 
|        |   POST   |         api/events         |      |         App\Http\Controllers\EventController@store         |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD |      api/events/{id}       |      |         App\Http\Controllers\EventController@show          |                    api                     | 
|        |  DELETE  |      api/events/{id}       |      |        App\Http\Controllers\EventController@destroy        |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        |   PUT    |      api/events/{id}       |      |        App\Http\Controllers\EventController@update         |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        |   POST   |         api/login          |      |       App\Http\Controllers\API\AuthController@login        |                    api                     |
|        |   POST   |         api/logout         |      |       App\Http\Controllers\API\AuthController@logout       |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD |       api/performers       |      |       App\Http\Controllers\PerformerController@index       |                    api                     |
|        |   POST   |       api/performers       |      |       App\Http\Controllers\PerformerController@store       |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD | api/performers/{performer} |      |       App\Http\Controllers\PerformerController@show        |                    api                     | 
|        |   PUT    | api/performers/{performer} |      |      App\Http\Controllers\PerformerController@update       |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        |  DELETE  | api/performers/{performer} |      |      App\Http\Controllers\PerformerController@destroy      |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        |   POST   |        api/register        |      |      App\Http\Controllers\API\AuthController@register      |                    api                     |
|        |   POST   |         api/stages         |      |         App\Http\Controllers\StageController@store         |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD |         api/stages         |      |         App\Http\Controllers\StageController@index         |                    api                     |
|        |   PUT    |     api/stages/{stage}     |      |        App\Http\Controllers\StageController@update         |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        |  DELETE  |     api/stages/{stage}     |      |        App\Http\Controllers\StageController@destroy        |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD |     api/stages/{stage}     |      |         App\Http\Controllers\StageController@show          |                    api                     |
|        | GET/HEAD |        api/tickets         |      |  App\Http\Controllers\TicketController@showTicketsForUser  |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            | App\Http\Middleware\EnsureUserRole:visitor |
|        |   POST   |        api/tickets         |      |        App\Http\Controllers\TicketController@store         |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            | App\Http\Middleware\EnsureUserRole:visitor |
|        |  DELETE  |    api/tickets/{ticket}    |      |       App\Http\Controllers\TicketController@destroy        |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        |          |                            |      |                                                            |  App\Http\Middleware\EnsureUserRole:admin  |
|        | GET/HEAD |          api/user          |      |                          Closure                           |                    api                     |
|        |          |                            |      |                                                            |  App\Http\Middleware\Authenticate:sanctum  |
|        | GET/HEAD |    sanctum/csrf-cookie     |      | Laravel\Sanctum\Http\Controllers\CsrfCookieController@show |                    web                     |

U tabeli iznad za svaku rutu možemo videti njene osnovne karakteristike po svakoj koloni:
- Method kolona opisuje koja HTTP metoda se koristi za tu rutu.
- URI kolona nam kazuje jedinstveni lokator resursa tj. na kojoj putanji se nalazi dati resurs.
- Name: alijas resursa.
- Action: koja se akcija izvršava kada korisnik pošalje odgovarajući zahtev za dati resurs.
- Middleware: Koji je posrednik u komunikaciji između korisničkog zahteva i akcije koja se izvršava.

#Karakteristični delovi koda

Ovde će biti prikazani delovi koda koji su karakteristični za ovu aplikaciju:

###Migracije

Migracije  prosto rečeno predstavljaju način za kontrolisanje verzija baze podataka. One omogućavaju da projektanti imaju lak pristup bazi podataka kao i da se lako dodaju izmene u DDL naredbama pri kreiranju baze na taj način da prethodna verzija baze podataka ostane netaknuta.

U sledećem primeru biće prikazan deo koda migracije za kreiranje tabele `events` u bazi:

```
class CreateEventsTable extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->dateTime('start');
            $table->foreignId('stage_id')->constrained()->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('user_id')->constrained()->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
        Schema::table('events', function (Blueprint $table){
            $table->dropForeign('stage_id');
            $table->dropForeign('user_id');
        });

    }
}
```

U prethodnom bloku koda prikazana je klasa `CreateEventsTable` koja sadrži `up()` i `down()` funkcije. Prva je odgovorna za kreiranje tabele events sa definisanim kolonima i ona s eizvršava prilikom poziva 
_php artisan migrate:(re)fresh_ komande. Funkcija _down()_ se poziva u momentu kada se poyove komanda `php artisan migrate:rollback` i ona uklanja datu tabelu iz baze.

###Eloquent model

Na primeru Event modela biće objašnjen Eloquent model:

```
/**
 * @mixin Builder
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'start', 'stage_id', 'performer_id', 'user_id'
    ];

    protected $casts = [
        'start' => 'date:Y-m-d H:i:s'
    ];

//    referenca na Stage
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function performers()
    {
        return $this->belongsToMany(Performer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
```

U prethodnom fragmentu koda vidimo kako izgleda klasa modela u Laravelu.
Modeli se uz pomoć **Artisan**-a kreiraju pomoću komande `php artisan make:model Event`
Svaki korisnički model mora da nasleđuje klasu Model. Deo `use HasFactory` je neophodan ukoliko 
tabelu u bazi želimo a popunjavamo pomoću Factory klase. Atributi koji su definisani kao `$fillable` mogu biti automatski popunjeni u tabeli u bazi preko **Seeder**-a.
Funkcije `stage()`, `performers()`, i `user()` definišu odnose sa ostalim Eloquent modelima (sa kojima su povezani u domenskom modelu).

###Kontroleri

```
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index(): string
    {
        $events = Event::all();
        return response()->json(EventResource::collection($events));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:App\Models\Event,name',
            'start' => 'required',
            'stage_id' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $event = Event::create($request->all());
        return response()->json(EventResource::make($event));
    }

    /**
     * Display the specified resource.
     *
* //     * @param  \App\Models\Event  $event
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        try {
            $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);
            return response()->json(new EventResource($event));
        }catch (Exception $exception){
            dd($exception);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Event $event
     * @return Response
     */
    public function edit(Event $event)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
//            'name' => 'required|unique:App\Models\Event,name',
            'name' => 'required',
            'start' => 'required',
            'stage_id' => 'required',
            'user_id' => 'required',
            'created_at' => '',
            'updated_at' => ''
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);

        $status = $event->update([
            'name' => $request->get('name'),
            'start'=>$request->input('start'),
            'stage_id'=>$request->get('stage_id'),
            'user_id' => $request->get('user_id')
        ]);

        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        // da bismo obrisali event moramo da obrisemo prvo njegovu vezu sa izvodjacem koja se nalazi u pivot tabeli
        $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);


        $event_performer = DB::table('event_performer')->where('event_id', $event->id)->delete();


        $status = $event->delete();
        return response()->json($status);
    }
}
```

U prethodnom segmentu koda predstavljen je Kontroler za događaje na festivalu.
Kontroler sadrži funkcije koje omogućavaju CRUD funkcionalnosti nad entitetom. Tako se 
npr. funkcija `index()` koristi za izlistavanje svih entiteta u tabeli, funkcija `show($id)` za 
prikaz jednog zapisa tabele na osnovu zadatog id-ja, `store(Request $req)` za dodavanje zapisa u tabelu,
`update(Request $request, $id)` za ažuriranje zapisa iz tabele, `destroy($id)` za brisanje zapisa iz tabele sa zadatim id-jem, itd.

###API rute

```
Route::middleware(['auth:sanctum', 'role:admin'])->group(function () {
    /*samo za admina*/

    Route::post('stages', [StageController::class, 'store']); //radi, samo mora da se pazi na ogranicenja pri unosu

    Route::post('performers', [PerformerController::class, 'store']); //radi, samo mora da se pazi na ogranicenja pri unosu

    Route::put('stages/{stage}', [StageController::class, 'update']); // ??

    Route::put('performers/{performer}', [PerformerController::class, 'update']); // ??

    Route::delete('stages/{stage}', [StageController::class, 'destroy']); // radi

    Route::delete('performers/{performer}', [PerformerController::class, 'destroy']); // radi

    Route::delete('tickets/{ticket}', [TicketController::class, 'destroy']); // radi

    Route::post('events', [EventController::class, 'store']); //radi, samo mora da se pazi na ogranicenja pri unosu

    Route::put('events/{id}', [EventController::class, 'update']); // radi ali ne mogu da se prosledjuju podaci kroz formu vec mora bas kao JSON

    Route::delete('events/{id}', [EventController::class, 'destroy']);  // radi

    Route::post('/logout', [AuthController::class, 'logout']); // radiii
});
```

Na osnovu gornjeg primera vidimo na koji način su definisane API rute web servisa. Najpre je 
definisan grupni posrednik (**middleware**) koji obezbeđuje da datim rutama može pristupiti samo 
korisnik sa ulogom administratora koji je autentifikovan - `middleware(['auth:sanctum', 'role:admin'])`.
Ruta se definiše tako što se nad klasom `Route` pozove statička metoda koja definiše tip zahteva 
na koji ruta odgovara - `delete`, `post`, `put`, `get` itd. Nakon tofa se kao prvi argument definiše putanja,
a kao drugi akcija koja se odigrava pri datom zahtevu. Akcije su definisane u odgovarajućim funkcijama kontrolera koje smo ranije objasnili.
