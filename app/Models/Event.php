<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'start', 'image', 'stage_id', 'performer_id', 'user_id'
    ];

    protected $casts = [
        'start' => 'date:Y-m-d H:i:s'
    ];

//    referenca na Stage
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function performers()
    {
        return $this->belongsToMany(Performer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
