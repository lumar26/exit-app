<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StageResource extends JsonResource
{

    public static $wrap = 'stage';

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'location' => $this->location,
            'sponsor' => $this->sponsor,
            'image' => $this->image,
            'capacity' => $this->capacity,
            'user' => UserResource::make($this->user)

//            'events' => EventResource::collection($this->events) ovo blokira aplikaciju jer se ulazi u beskonačnu rekurziju
        ];
    }
}
