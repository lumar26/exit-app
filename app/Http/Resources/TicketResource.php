<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    public static $wrap = 'ticket';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'purchase_date' => $this->purchase_date,
            'price' => $this->price,
            'discount'=> $this->discount,
            'stage' => StageResource::make($this->stage),
            'user' => UserResource::make($this->user)
        ];
    }
}
