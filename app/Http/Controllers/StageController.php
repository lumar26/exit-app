<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Http\Resources\StageResource;
use App\Models\Event;
use App\Models\Stage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Util\Exception;

class StageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $stages = Stage::all();
        return response()->json(StageResource::collection($stages));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:App\Models\Stage,name',
            'location' => 'required',
            'capacity' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $stage = Stage::create($request->all());
        return response()->json(StageResource::make($stage));
    }

    /**
     * Display the specified resource.
     *
     * @param Stage $stage
     * @return JsonResponse
     */
    public function show(Stage $stage)
    {
        try {
            return response()->json(new StageResource($stage));
        }catch (Exception $exception){
            return response()->json(['exception' => $exception], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Stage $stage
     * @return \Illuminate\Http\Response
     */
    public function edit(Stage $stage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Stage $stage
     * @return JsonResponse
     */
    public function update(Request $request, Stage $stage)
    {
        $validator = Validator::make($request->all(), [
           'name' => 'required|unique:App\Models\Stage,name',
            'location' => 'required',
            'capacity' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

//        $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);

        $status = $stage->update([
            'name' => $request->get('name'),
            'location'=>$request->input('location'),
            'capacity'=>$request->get('capacity'),
            'sponsor'=>$request->get('sponsor'),
            'user_id' => $request->get('user_id')
        ]);

        return response()->json($stage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Stage $stage
     * @return JsonResponse
     */
    public function destroy(Stage $stage)
    {
        $status = $stage->delete();
        return response()->json($status);
    }
}
