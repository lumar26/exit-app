<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Util\Exception;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $tickets = Ticket::all();
        return response()->json(TicketResource::collection($tickets));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'stage_id' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $ticket = Ticket::create($request-> all());
        return response()->json(TicketResource::make($ticket));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return JsonResponse
     */
    public function show(Ticket $ticket)
    {
        try {
//            $ticket = Ticket::findOrFail($id); //eager loading
            return response()->json(new TicketResource($ticket));
        }catch (Exception $exception){
            return response()->json(['exception' => $exception], 400);
        }
    }

    /**
     * Display tickets of user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function showTicketsForUser(Request $request): JsonResponse
    {
        try {
            $tickets = Ticket::where('user_id', $request->user()->id)->get(); //eager loading
            return response()->json(TicketResource::collection($tickets));
        }catch (Exception $exception){
            return response()->json(['exception' => $exception], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Models\Ticket  $ticket
     * @return JsonResponse
     */
    public function update(Request $request, Ticket $ticket)
    {
        $request->validate([
            'price'=>'required',
        ]);

        $status = $ticket->update([
            'price' => $request->price,
            'discount' => $request-> discount,
            'purchase_date' => $request->purchase_date,
        ]);

        return response ()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return JsonResponse
     */
    public function destroy(Ticket $ticket)
    {
        $status = $ticket->delete();
        return response()->json($status);
    }
}
