<?php

namespace App\Http\Controllers;

use App\Http\Resources\PerformerResource;
use App\Models\Performer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Util\Exception;

class PerformerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $performers = Performer::all();
        return response()->json(PerformerResource::collection($performers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nick' => 'required|unique:App\Models\Stage,name',
            'name' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $performer = Performer::create($request->all());
        return response()->json(PerformerResource::make($performer));
    }

    /**
     * Display the specified resource.
     *
     * @param Performer $performer
     * @return JsonResponse
     */
    public function show(Performer $performer): JsonResponse
    {
        try {
            return response()->json(new PerformerResource($performer));
        } catch (Exception $exception) {
            return response()->json(['exception' => $exception], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Performer $performer
     * @return \Illuminate\Http\Response
     */
    public function edit(Performer $performer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Performer $performer
     * @return JsonResponse
     */
    public function update(Request $request, Performer $performer)
    {
        $request->validate([
            'nick' => 'required',

        ]);

        $status = $performer->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'nick' => $request->nick,
            'music_genre' => $request->music_genre
        ]);

        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Performer $performer
     * @return JsonResponse
     */
    public function destroy(Performer $performer): JsonResponse
    {
        $status = $performer->delete();
//        return response()->json($status);
        return response()->json($performer);
    }
}
