<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:150',
            'email' => 'required|string|email',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'role' => $request->get('role'),
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json(['user' => $user, 'accessToken' => $token, 'tokenType' => 'Bearer']);
    }

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        //ovde cemo da proveravamo autorizovanog korisnika
        //koristicemo klasu Auth koja se koristi za pristup autentifikovanom korisniku
        //kazemo okej proveri da li taj korisnik postoji, ko je taj korisnik
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json(['message' => 'Login failed: email or password not provided.' . 'password: ' . $request->get('password')], 400);
        }
        //vrati mi usera ciji je email zapravo ovaj email koji smo dobili u requestu, a ukoliko ih ima vise,
        //vrati mi prvog
        $user = User::where('email', $request->get('email'))->firstOrFail();

        //registrovali smo se, dobili token, pa smo mogli da se ulogujemo, a sada cemo dobiti novi token i sa njim cemo
        //moci da se krecemo kroz aplikaciju
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json(['user' => $user, 'accessToken' => $token, 'tokenType' => 'Bearer']);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->user()->tokens()->delete();
        return response()->json(['logout_msg' => 'Uspešno ste se odjavili i token je uspešno obrisan.']);
    }
}
